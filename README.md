# Deploy Self-managed GitLab



## Getting started

This will help person they need to deploy a Self-managed Gitlab in their infrastructure or on a public cloud !

## Gitlab Components
| Component               | Description                                                      | Can be Externalized? |
|-------------------------|------------------------------------------------------------------|----------------------|
| GitLab Web Service      | Handles the web UI, API, and Git over HTTP(S).                   | Yes                  |
| GitLab Shell            | Manages Git SSH sessions.                                        | No (co-located)      |
| GitLab Workhorse        | Reverse proxy server for handling large HTTP requests.           | No (co-located)      |
| PostgreSQL Database     | Primary database for storing data.                               | Yes                  |
| Redis                   | Used for caching and job queues.                                 | Yes                  |
| Gitaly                  | Manages Git repository access and storage.                       | Yes                  |
| GitLab Runner           | Runs CI/CD jobs.                                                 | Yes                  |
| Sidekiq                 | Processes background jobs.                                       | Yes (scalable)       |
| Prometheus & Grafana    | Monitoring tools for GitLab and infrastructure.                  | Yes                  |
| Object Storage (S3 etc.)| Storage for artifacts, LFS, and backups.                         | Yes                  |
| Load Balancer           | Distributes traffic across application servers.                  | Yes (external)       |
| Elasticsearch           | Provides advanced search functionality.                          | Yes                  |
| SMTP Server             | Used for sending emails.                                         | Yes                  |
| Backup & Disaster Recovery | Systems for data protection and recovery.                       | Yes                  |
